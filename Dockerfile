FROM python:3-alpine
WORKDIR /DesCipherer
COPY . .
RUN apk add gcc python3-dev build-base libffi-dev
RUN pip install --no-cache-dir -r req-srv.txt
CMD python bot.py
