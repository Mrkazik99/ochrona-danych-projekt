import random
import yaml

from telethon import TelegramClient, events
from DES import DES

with open("config.yaml", "r") as config:
    cfg = yaml.safe_load(config)


client = TelegramClient(**cfg['telethon_settings']).start(bot_token=cfg['bot_token'])
config.close()
des = DES()


@client.on(events.InlineQuery)
async def querylist(event):
    if len(event.text) > 2 and len(event.text) < 257:
        if event.text[-2:] == '.c':
            key = str(random.randint(10000000, 99999999))
            ciph = des.run(key=key, text=event.text[:-2], encrypt=True).encode("utf-8").hex()

            await event.answer(
                [event.builder.article('cipher',
                                       text=f'```Ciphered: {ciph}'
                                            f'\nCiphered with key: {key}'
                                            f'\nCopy and paste to decipher: {key} {ciph}.d```')])

        elif event.text[-2:] == '.d':
            data = event.text.split(' ')

            if len(data[0]) == 8:
                await event.answer([event.builder.article(
                    f'decipher', text=f'```Key: {data[0]}\n'
                                      f'Deciphered: {des.run(key=data[0], text=bytes.fromhex(data[1][:-2]).decode("utf-8"), encrypt=False)}```')])
            else:
                await event.answer([event.builder.article('**KEY ERROR**', text='Key is not 8 signs long')])

        else:
            await event.answer([event.builder.article('Wrong switch',
                                                      text=f'**USAGE**:\n__message.c__ to cipher'
                                                           f'\n__key + message.d__ to decipher')])

    else:
        await event.answer([event.builder.article('**USAGE**',
                                                  text=f'**USAGE**:\n__message.c__ to cipher'
                                                       f'\n__key + message.d__ to decipher\nmessage to cipher must be upto 255 chars long (without switch)')])


@client.on(events.NewMessage)
async def answer(event):
    if len(event.text) > 2 and len(event.text) < 257:
        if event.text[-2:] == '.c':
            key = str(random.randint(10000000, 99999999))
            ciph = (des.run(key=key, text=event.text[:-2], encrypt=True)).encode("utf-8").hex()
            await event.reply(
                f'```Ciphered: {ciph}'
                f'\nCiphered with key: {key}'
                f'\nCopy and paste to decipher: {key} {ciph}.d```')

        elif event.text[-2:] == '.d':
            data = event.text.split(' ')
            if len(data[0]) == 8:
                deciph = des.run(key=data[0], text=bytes.fromhex(data[1][: -2]).decode("utf-8"), encrypt=False)
                await event.reply(f'```Key: {data[0]}\n'
                                  f'Deciphered: {deciph}```')

            else:
                await event.reply('**KEY ERROR!**\nKey is not 8 signs long')

    else:
        await event.reply('**USAGE**:\n__message.c__ to cipher\n__key + message.d__ to decipher\nmessage to cipher must be upto 255 chars long (without switch)')


client.start()
client.run_until_disconnected()
